#include <stdio.h>

int main(int argc, char *argv[]) {
	int distance = 100;
	float power = 2.345f;
	double super_power = 5674.324;
	char initial = 'A';
	char first_name[] = "Zed";
	char last_name[] = "Shaw";

	printf("You are %d miles away.\n", distance);
	printf("You have %f levels of power.\n", power);
	printf("You have %f awesome super powers.\n", super_power);
	printf("I have an initial %c.\n", initial);
	printf("My name is %s %c. %s.\n", first_name, initial, last_name);

	// extra credit

	char empty_string[] = "";
	printf("%s", empty_string);

	int hex_number = 0x10;
	printf("I am %d years old.\n", hex_number);

	return 1;
}