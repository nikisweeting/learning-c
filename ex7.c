#include <stdio.h>

int main(int argc, char *argv[]) {

	int bugs = 100;
	double bug_rate = 1.2;

	printf("You have %d at the rate of %f/second.\n", 
		bugs, bug_rate);

	long universe_of_defects = 1L * 1024L * 1024L * 1024L;
	printf("The entire universe has %ld bugs.\n",
		universe_of_defects);

	double expected_bugs = bugs * bug_rate;
	printf("You are expected to have %f bugs.\n",
		expected_bugs);

	double part_of_the_universe = expected_bugs / universe_of_defects;
	printf("That is only a %e portion of the universe.\n", 
		part_of_the_universe);

	//makes no sense, just a demo
	char nul_byte = '\0';
	int care_percentage = bugs * nul_byte;
	printf("Which means you should care %d%%.\n", care_percentage);

	// extra credit

	char test = 'a'; // evals to 97 as an int... funky
	int two = 2;
	double final = two * test;
	printf("test: %d, two: %d, final %d.\n",
		test, two, final);

	return 0;
}