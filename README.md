### Learning C the Hard Way

This is just my personal version tracking for some C, and Obj-C exercises I'm working through. Most of them are from this book:

http://c.learncodethehardway.org/book/ex8.html  

Some more useful links:  

http://valgrind.org/  

https://github.com/nikisweeting/learning-c.git

http://learncodethehardway.org  

http://cocoadevcentral.com/d/learn_objectivec/  

http://learnyouahaskell.com/starting-out  

http://tryobjectivec.codeschool.com (/levels/2/challenges/7)<--last challenge completed
