#include <stdio.h>

int main() {
	int age = 16;
	float height = 1.8;

	printf("I am %d years old.\n", age);
	printf("I am %d meters tall.\n", height);

	printf("I am %g meters tall.\a\n", height);

	return 0;
}